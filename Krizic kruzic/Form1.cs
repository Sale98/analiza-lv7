﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Krizic_kruzic
{
    public partial class Form1 : Form
    {
        bool potez = true;
        int brojpoteza = 0;
        static string igrac1,igrac2 ="";
        public Form1()
        {
            InitializeComponent();
        }
        public static void imenaigraca(string a,string b)
        {
            igrac1 = a;
            igrac2 = b;
        }
        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (potez)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "O";
            }
            potez = !potez;
            b.Enabled = false;
            brojpoteza++;
            provjeri_pobjednika();
            
        }

        private void provjeri_pobjednika()
        {
            
            bool ima_pobjednik = false;
            //vodoravna provjera
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
            { ima_pobjednik = true; }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
            { ima_pobjednik = true; }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
            { ima_pobjednik = true; }
            //okomita provjera
            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
            { ima_pobjednik = true; }
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
            { ima_pobjednik = true; }
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
            { ima_pobjednik = true; }
            //dijagonalna provjera
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
            { ima_pobjednik = true; }
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!A3.Enabled))
            { ima_pobjednik = true; }


            if (ima_pobjednik)
            {
                DisableButtons();
                if (potez)
                {
                    igrac1 = "O";
                    O_pobjeda.Text = (Int32.Parse(O_pobjeda.Text) + 1).ToString();
                    MessageBox.Show(label2.Text + " Pobjednik!");
                }
                else
                {
                    igrac2 = "X";
                    x_pobjeda.Text = (Int32.Parse(x_pobjeda.Text) + 1).ToString();
                    MessageBox.Show(label1.Text + " Pobjednik!");
                }
               
            }
            
            else
            {
                if(brojpoteza==9)
                {
                    jednako.Text = (Int32.Parse(jednako.Text) + 1).ToString();
                    MessageBox.Show("Izjednaceno");
                }
            }
            
          
        }
        private void DisableButtons()
        {
            try
            { 
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { }
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            potez = true;
            brojpoteza = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
        }

        private void button_enter(object sender, EventArgs e)
        {

        }
        private void button_leave(object sender, EventArgs e)
        {

        }

        private void obrisiPobjedeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            x_pobjeda.Text = "0";
            O_pobjeda.Text = "0";
            jednako.Text = "0";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            label1.Text = igrac1;
            label2.Text = igrac2;
        }
    }
}

